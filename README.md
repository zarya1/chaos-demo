# Chaos Demo

demo using chaostoolkit for chaos engineering 

## Install chaostoolkit

[click here for instructions](https://github.com/chaostoolkit/chaostoolkit/)

## install k8s plugin

1. pip install -U chaostoolkit-kubernetes

2. chaos discover chaostoolkit-kubernetes

3. cat discovery.json

## run a pod 

1. kubectl create ns caf

2. kubectl apply -f deployment.yaml

## run the demo experiment

1. chaos run chaos/terminate-pod.yaml